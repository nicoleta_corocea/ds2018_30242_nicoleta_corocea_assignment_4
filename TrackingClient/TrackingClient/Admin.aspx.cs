﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TrackingClient
{
    public partial class Admin : System.Web.UI.Page
    {
        DotNetWebService.user user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                user = (DotNetWebService.user)Session["user"];
                if (user.usertype.Equals("client"))
                {
                    Response.Redirect("Error.aspx");
                }
            }
            else
            {
                Response.Redirect("Error.aspx");
            }

            helloUser.Text = "Hello Admin       ";

            listAllForm.Visible = false;
            addForm.Visible = false;
            deleteForm.Visible = false;
            registerForm.Visible = false;
            updateStatusForm.Visible = false;
            addErrorLabel.Visible = false;

        }

        protected void addPackBtn_Click(object sender, EventArgs e)
        {
            addForm.Visible = true;
            addErrorLabel.Visible = false;
            successAddLabel.Visible = false;
            if (senderUsernameBox.Text.ToString().Length == 0 || recUsernameBox.Text.ToString().Length == 0 || nameBox.Text.ToString().Length == 0 || descriptionBox.Text.ToString().Length == 0 || senderCityBox.Text.ToString().Length == 0 || destinationCityBox.Text.ToString().Length == 0)
            {
                addErrorLabel.Visible = true;
                addErrorLabel.Text = "Missing data";
                return;
            }
            TrackingClient.JavaWebService.TrackingWebServiceClient javaWebService = new TrackingClient.JavaWebService.TrackingWebServiceClient();
            JavaWebService.package p = new JavaWebService.package();
            p.sender = new JavaWebService.user();
            p.receiver = new JavaWebService.user();
            p.sender.userName = senderUsernameBox.Text.ToString();
            p.receiver.userName = recUsernameBox.Text.ToString();
            p.packageName = nameBox.Text.ToString();
            p.packageDescription = descriptionBox.Text.ToString();
            p.senderCity = senderCityBox.Text.ToString();
            p.destinationCity = destinationCityBox.Text.ToString();
            int idPackage = javaWebService.addPackage(p);

            if (idPackage == -1)
            {
                addErrorLabel.Visible = true;
                addErrorLabel.Text = "Something went wrong because some data are incorrect!";
                return;
            }
            successAddLabel.Visible = true;
            successAddLabel.Text = "Package added successfully";

        }

        protected void allBtn_Click(object sender, EventArgs e)
        {
            listAllForm.Visible = true;
            addForm.Visible = false;
            deleteForm.Visible = false;
            registerForm.Visible = false;
            updateStatusForm.Visible = false;

            TrackingClient.JavaWebService.TrackingWebServiceClient javaWebService = new TrackingClient.JavaWebService.TrackingWebServiceClient();
            JavaWebService.package[] packages = javaWebService.getAllPackages();

            foreach (JavaWebService.package p in packages)
            {
                TableRow row = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = p.id.ToString();
                row.Cells.Add(idCell);
                TableCell senderCell = new TableCell();
                senderCell.Text = p.sender.userName;
                row.Cells.Add(senderCell);
                TableCell receiverCell = new TableCell();
                receiverCell.Text = p.receiver.userName;
                row.Cells.Add(receiverCell);
                TableCell packageCell = new TableCell();
                packageCell.Text = p.packageName;
                row.Cells.Add(packageCell);
                TableCell descriptionCell = new TableCell();
                descriptionCell.Text = p.packageDescription;
                row.Cells.Add(descriptionCell);
                TableCell city1Cell = new TableCell();
                city1Cell.Text = p.senderCity;
                row.Cells.Add(city1Cell);
                TableCell city2Cell = new TableCell();
                city2Cell.Text = p.destinationCity;
                row.Cells.Add(city2Cell);
                TableCell trackingCell = new TableCell();
                trackingCell.Text = p.tracking.ToString();
                row.Cells.Add(trackingCell);
                packageTable.Rows.Add(row);

            }
        }

        protected void addBtn_Click(object sender, EventArgs e)
        {
            listAllForm.Visible = false;
            addForm.Visible = true;
            deleteForm.Visible = false;
            registerForm.Visible = false;
            updateStatusForm.Visible = false;
            successAddLabel.Visible = false;
        }

        protected void deleteBtn_Click(object sender, EventArgs e)
        {
            listAllForm.Visible = false;
            addForm.Visible = false;
            deleteForm.Visible = true;
            registerForm.Visible = false;
            updateStatusForm.Visible = false;
            deleteErrorLabel.Visible = false;
            deletedLabel.Visible = false;

        }
        protected void registerClick(object sender, EventArgs e)
        {
            {
                listAllForm.Visible = false;
                addForm.Visible = false;
                deleteForm.Visible = false;
                registerForm.Visible = true;
                registerErrorLabel.Visible = false;
                registerLabel.Visible = false;
            }
        }

        protected void signOut_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Login.aspx");
        }

        protected void Delete_Package_btn(object sender, EventArgs e)
        {
            deleteForm.Visible = true;
            deleteErrorLabel.Visible = false;
            deletedLabel.Visible = false;
            int packageIdInput;
            if (!int.TryParse(deletePackageIdBox.Text.ToString(), out packageIdInput) || packageIdInput == 0)
            {
                deleteErrorLabel.Visible = true;
                deleteErrorLabel.Text = "The id is not valid";
                return;
            }

            TrackingClient.JavaWebService.TrackingWebServiceClient javaWebService = new TrackingClient.JavaWebService.TrackingWebServiceClient();
            JavaWebService.package deletedPackage = javaWebService.removePackage(packageIdInput);
            if (deletedPackage == null)
            {
                deleteErrorLabel.Visible = true;
                deleteErrorLabel.Text = "The id is not valid";
                return;
            }
            deletedLabel.Visible = true;
            deletedLabel.Text = "The package was deleted";

        }

        protected void Register_Package_btn(object sender, EventArgs e)
        {
            registerForm.Visible = true;
            registerErrorLabel.Visible = false;
            registerLabel.Visible = false;
            int packageIdInput;
            if (!int.TryParse(registerPackageIdBox.Text.ToString(), out packageIdInput) || packageIdInput == 0)
            {
                registerErrorLabel.Visible = true;
                registerErrorLabel.Text = "The id is not valid";
                return;
            }

            TrackingClient.JavaWebService.TrackingWebServiceClient javaWebService = new TrackingClient.JavaWebService.TrackingWebServiceClient();
            JavaWebService.package registerPackage = javaWebService.registerPackage(packageIdInput);
            if (registerPackage == null)
            {
                registerErrorLabel.Visible = true;
                registerErrorLabel.Text = "The id is invalid or this package has been already registered";
                return;
            }
            registerLabel.Visible = true;
            registerLabel.Text = "The tracking fot this package is enabled";

        }

        protected void updateClick(object sender, EventArgs e)
        {
            listAllForm.Visible = false;
            addForm.Visible = false;
            deleteForm.Visible = false;
            registerForm.Visible = false;
            updateStatusForm.Visible = true;
            deleteErrorLabel.Visible = false;
            deletedLabel.Visible = false;
            updatedLabel.Visible = false;
            updateErrorLabel.Visible = false;
        }

        protected void addPairBtn_Click(object sender, EventArgs e)
        {
            updateStatusForm.Visible = true;
            updatedLabel.Visible = false;
            updateErrorLabel.Visible = false;

            int packageIdInput;
            if (!int.TryParse(pairPackageIdBox.Text.ToString(), out packageIdInput) || packageIdInput == 0)
            {
                updateErrorLabel.Visible = true;
                updateErrorLabel.Text = "The id is not valid";
                return;
            }

            string cityName = pairCityBox.Text.ToString();
            if (cityName.Length == 0)
            {
                updateErrorLabel.Visible = true;
                updateErrorLabel.Text = "The city is not valid";
                return;
            }
            string dataString = pairTimeBox.Text.ToString();


            try
            {
                DateTime tryParse = DateTime.ParseExact(dataString, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                updateErrorLabel.Visible = true;
                updateErrorLabel.Text = "Date is not valid. Try yyyy-MM-dd HH:mm:ss format";
                return;
            }
            DateTime myDate = DateTime.ParseExact(dataString, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

            long unixTimestamp = (long)(myDate.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            TrackingClient.JavaWebService.TrackingWebServiceClient javaWebService = new TrackingClient.JavaWebService.TrackingWebServiceClient();
            int addPairRsp = javaWebService.addPairToPackageRoute(packageIdInput, cityName, unixTimestamp);
            if (addPairRsp == -1)
            {
                updateErrorLabel.Visible = true;
                updateErrorLabel.Text = "The package route could not be updated";
                return;
            }
            updatedLabel.Visible = true;
            updatedLabel.Text = "Package route updated with success.";
        }
    }
}