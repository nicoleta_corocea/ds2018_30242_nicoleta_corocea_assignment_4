﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TrackingClient
{
    public partial class Login : System.Web.UI.Page
    {
    

        protected void Button1_Click(object sender, EventArgs e)
        {

            TrackingClient.DotNetWebService.TrackingServiceSoapClient dotNetService = new TrackingClient.DotNetWebService.TrackingServiceSoapClient();
            string username = usernameBox.Text.ToString();
            string password = passwordBox.Text.ToString();
            if(username.Length == 0 || password.Length == 0)
            {
                errrorLabel.Text = "Bad credentials. Please try again";
                return;
            }
            DotNetWebService.user user = null;
            try
            {
                user = dotNetService.login(usernameBox.Text.ToString(), passwordBox.Text.ToString());
            }
            catch
            {
                errrorLabel.Text = "The webservice is closed. Please turn it on";
                return;
            }

            if (user == null)
            {
                errrorLabel.Text = "Bad credentials";
            }
            else
            {
                Session["user"] = user;
                if (user.usertype.Equals("client"))
                {
                    Response.Redirect("Client.aspx");
                }
                else
                {
                    Response.Redirect("Admin.aspx");
                }
            }

        }

        protected void registerBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }
    }
}