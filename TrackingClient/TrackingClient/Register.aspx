﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="TrackingClient.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register page</title>
    <style type="text/css">
        .auto-style1 {
            height: 243px;
        }
        .auto-style2 {
            margin-left: 13px;
        }
        .auto-style3 {
            margin-left: 15px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style1">
            <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label>
            <asp:TextBox ID="regUsernameBox" runat="server" CssClass="auto-style2" Width="144px"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
            <asp:TextBox ID="regPasswordBox" runat="server" CssClass="auto-style3" Width="142px"></asp:TextBox>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="saveUserBtn" runat="server" OnClick="saveUserBtn_Click" Text="Save" Width="67px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Back to Login" Width="101px" />
            <br />
            <asp:Label ID="error" runat="server" ForeColor="#FF3300"></asp:Label>
        </div>
    </form>
</body>
</html>
