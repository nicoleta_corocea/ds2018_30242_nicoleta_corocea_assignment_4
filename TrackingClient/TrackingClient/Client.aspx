﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Client.aspx.cs" Inherits="TrackingClient.Client" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Client</title>
    </head>
<body>
    <form runat="server">
        <p>
            <asp:Label ID="helloUser" runat="server" Text="Label"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="signOut" runat="server" OnClick="signOut_Click">SignOut</asp:LinkButton>
        </p>
        <p>Packages</p>
    <asp:Button onclick="listBtn_Click" ID="listBtn" runat="server" Text="List all packages" />
    <asp:Button onclick="searchBtn_Click" ID="searchBtn" runat="server" Text="Search package" Width="152px" />
    <asp:Button onclick="checkBtn_Click" ID="checkBtn" runat="server" Text="Check status" />
    


	<div id="listForm" runat="server">
        <asp:Label ID="Label1" runat="server" Text="I am the"></asp:Label>
        <br />
        <asp:Button ID="senderBtn" runat="server" OnClick="senderCheck_Click" Text="Sender" />
        <asp:Button ID="receivedBtn" runat="server" OnClick="receiverCheck_Click" Text="Receiver" />
        <br />
        <asp:Label ID="errorList" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <asp:Table ID="packageTable" runat="server" BackColor="White" BorderColor="Black" 
    BorderWidth="1" ForeColor="Black" GridLines="Both" BorderStyle="Solid" CellPadding="8">
            <asp:TableHeaderRow >
                <asp:TableCell Font-Bold="True">Id</asp:TableCell>
                <asp:TableCell Font-Bold="True">Sender</asp:TableCell>
                <asp:TableCell Font-Bold="True">Receiver</asp:TableCell>
                <asp:TableCell Font-Bold="True">Package name</asp:TableCell>
                <asp:TableCell Font-Bold="True">Description</asp:TableCell>
                <asp:TableCell Font-Bold="True">Sender city</asp:TableCell>
                <asp:TableCell Font-Bold="True">Destination city</asp:TableCell>
                <asp:TableCell Font-Bold="True">Tracking</asp:TableCell>
            </asp:TableHeaderRow>
        </asp:Table>
        <br />
        <br />
    </div>

	<div id="searchForm" runat="server">
		
	    <br />
		
	    <asp:Label ID="Label2" runat="server" Text="Package name: "></asp:Label>
        <asp:TextBox ID="searchBox" runat="server"></asp:TextBox>
        <asp:Button ID="searchPackageBtn" runat="server" OnClick="Button1_Click1" Text="Search" />
        <br />
        <br />
        <asp:Label ID="errorSearch" runat="server" ForeColor="Red"></asp:Label>
         <asp:Table ID="nameSearchTable" runat="server" BackColor="White" BorderColor="Black" 
    BorderWidth="1" ForeColor="Black" GridLines="Both" BorderStyle="Solid" CellPadding="8">
            <asp:TableHeaderRow >
                <asp:TableCell Font-Bold="True">Id</asp:TableCell>
                <asp:TableCell Font-Bold="True">Sender</asp:TableCell>
                <asp:TableCell Font-Bold="True">Receiver</asp:TableCell>
                <asp:TableCell Font-Bold="True">Package name</asp:TableCell>
                <asp:TableCell Font-Bold="True">Description</asp:TableCell>
                <asp:TableCell Font-Bold="True">Sender city</asp:TableCell>
                <asp:TableCell Font-Bold="True">Destination city</asp:TableCell>
                <asp:TableCell Font-Bold="True">Tracking</asp:TableCell>
            </asp:TableHeaderRow>
        </asp:Table>
        <br />
        </div>

	<div id="checkForm" runat="server">
	
        <br />
	
	    <asp:Label ID="Label3" runat="server" Text="Package id: "></asp:Label>
        <asp:TextBox ID="checkPackageIdBox" runat="server"></asp:TextBox>
        <asp:Button ID="checkPackageBtn" runat="server" OnClick="Check_Package_btn" Text="Search" />
        <br />
        <br />
	
	    <asp:Label ID="errorCheck" runat="server" ForeColor="Red"></asp:Label>
        <asp:Label ID="routeErrorLabel" runat="server"></asp:Label>
        <br />
        <asp:Table ID="routeTable" runat="server" BackColor="White" BorderColor="Black" 
    BorderWidth="1" ForeColor="Black" GridLines="Both" BorderStyle="Solid" CellPadding="8">
            <asp:TableHeaderRow >
                <asp:TableCell Font-Bold="True">City</asp:TableCell>
                <asp:TableCell Font-Bold="True">Time</asp:TableCell>
            </asp:TableHeaderRow>
        </asp:Table>
        </div>
    </form>
</body>
</html>
</body>
</html>
