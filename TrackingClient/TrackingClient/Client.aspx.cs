﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrackingClient.Utils;

namespace TrackingClient
{
    public partial class Client : System.Web.UI.Page
    {
        DotNetWebService.user user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                user = (DotNetWebService.user)Session["user"];
                if (user.usertype.Equals("admin"))
                {
                    Response.Redirect("Error.aspx");
                }
            }
            else
            {
                Response.Redirect("Error.aspx");
            }

            helloUser.Text = "Hello "+ user.username + "      ";

            listForm.Visible = false;
            searchForm.Visible = false;
            checkForm.Visible = false;
        }

        protected void senderCheck_Click(object sender, EventArgs e)
        {
            listForm.Visible = true;
            TrackingClient.DotNetWebService.TrackingServiceSoapClient dotNetService = new TrackingClient.DotNetWebService.TrackingServiceSoapClient();
            List<DotNetWebService.package> packages = dotNetService.getAllPackagesByUserId(user.id);
            List<DotNetWebService.package> packagesReceive = new List<DotNetWebService.package>();

            foreach (DotNetWebService.package p in packages)
            {
                if (p.id_receiver == user.id)
                {
                    packagesReceive.Add(p);

                    TableRow row = new TableRow();
                    TableCell idCell = new TableCell();
                    idCell.Text = p.id.ToString();
                    row.Cells.Add(idCell);
                    TableCell senderCell = new TableCell();
                    senderCell.Text = p.sender.username;
                    row.Cells.Add(senderCell);
                    TableCell receiverCell = new TableCell();
                    receiverCell.Text = p.receiver.username;
                    row.Cells.Add(receiverCell);
                    TableCell packageCell = new TableCell();
                    packageCell.Text = p.package_name;
                    row.Cells.Add(packageCell);
                    TableCell descriptionCell = new TableCell();
                    descriptionCell.Text = p.package_description;
                    row.Cells.Add(descriptionCell);
                    TableCell city1Cell = new TableCell();
                    city1Cell.Text = p.sender_city;
                    row.Cells.Add(city1Cell);
                    TableCell city2Cell = new TableCell();
                    city2Cell.Text = p.destination_city;
                    row.Cells.Add(city2Cell);
                    TableCell trackingCell = new TableCell();
                    trackingCell.Text = p.tracking.ToString();
                    row.Cells.Add(trackingCell);
                    packageTable.Rows.Add(row);
                }

            }
        }

        protected void receiverCheck_Click(object sender, EventArgs e)
        {
            listForm.Visible = true;
            TrackingClient.DotNetWebService.TrackingServiceSoapClient dotNetService = new TrackingClient.DotNetWebService.TrackingServiceSoapClient();
            List<DotNetWebService.package> packages = dotNetService.getAllPackagesByUserId(user.id);
            List<DotNetWebService.package> packagesSend = new List<DotNetWebService.package>();


            foreach (DotNetWebService.package p in packages)
            {
                if (p.id_sender == user.id)
                {
                    packagesSend.Add(p);

                    TableRow row = new TableRow();
                    TableCell idCell = new TableCell();
                    idCell.Text = p.id.ToString();
                    row.Cells.Add(idCell);
                    TableCell senderCell = new TableCell();
                    senderCell.Text = p.sender.username;
                    row.Cells.Add(senderCell);
                    TableCell receiverCell = new TableCell();
                    receiverCell.Text = p.receiver.username;
                    row.Cells.Add(receiverCell);
                    TableCell packageCell = new TableCell();
                    packageCell.Text = p.package_name;
                    row.Cells.Add(packageCell);
                    TableCell descriptionCell = new TableCell();
                    descriptionCell.Text = p.package_description;
                    row.Cells.Add(descriptionCell);
                    TableCell city1Cell = new TableCell();
                    city1Cell.Text = p.sender_city;
                    row.Cells.Add(city1Cell);
                    TableCell city2Cell = new TableCell();
                    city2Cell.Text = p.destination_city;
                    row.Cells.Add(city2Cell);
                    TableCell trackingCell = new TableCell();
                    trackingCell.Text = p.tracking.ToString();
                    row.Cells.Add(trackingCell);
                    packageTable.Rows.Add(row);
                }

            }

        }

        protected void listBtn_Click(object sender, EventArgs e)
        {
            listForm.Visible = true;
            searchForm.Visible = false;
            checkForm.Visible = false;
            errorList.Visible = false;


        }

        protected void searchBtn_Click(object sender, EventArgs e)
        {
            listForm.Visible = false;
            searchForm.Visible = true;
            checkForm.Visible = false;
            errorSearch.Visible = false;


        }

        protected void checkBtn_Click(object sender, EventArgs e)
        {
            listForm.Visible = false;
            searchForm.Visible = false;
            checkForm.Visible = true;
            errorCheck.Visible = false;
            routeTable.Visible = false;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            searchForm.Visible = true;
            string packageNameInput = searchBox.Text.ToString();
            TrackingClient.DotNetWebService.TrackingServiceSoapClient dotNetService = new TrackingClient.DotNetWebService.TrackingServiceSoapClient();
            List<DotNetWebService.package> packages = dotNetService.getAllPackagesByUserIdAndPackageName(user.id, packageNameInput);

            foreach (DotNetWebService.package p in packages)
            {
                TableRow row = new TableRow();
                TableCell idCell = new TableCell();
                idCell.Text = p.id.ToString();
                row.Cells.Add(idCell);
                TableCell senderCell = new TableCell();
                senderCell.Text = p.sender.username;
                row.Cells.Add(senderCell);
                TableCell receiverCell = new TableCell();
                receiverCell.Text = p.receiver.username;
                row.Cells.Add(receiverCell);
                TableCell packageCell = new TableCell();
                packageCell.Text = p.package_name;
                row.Cells.Add(packageCell);
                TableCell descriptionCell = new TableCell();
                descriptionCell.Text = p.package_description;
                row.Cells.Add(descriptionCell);
                TableCell city1Cell = new TableCell();
                city1Cell.Text = p.sender_city;
                row.Cells.Add(city1Cell);
                TableCell city2Cell = new TableCell();
                city2Cell.Text = p.destination_city;
                row.Cells.Add(city2Cell);
                TableCell trackingCell = new TableCell();
                trackingCell.Text = p.tracking.ToString();
                row.Cells.Add(trackingCell);
                nameSearchTable.Rows.Add(row);

            }
        }

        protected void Check_Package_btn(object sender, EventArgs e)
        {
            checkForm.Visible = true;
            errorCheck.Visible = false;
            routeErrorLabel.Visible = false;
            routeTable.Visible = false;
            int packageIdInput;
            if (!int.TryParse(checkPackageIdBox.Text.ToString(), out packageIdInput) || packageIdInput == 0)
            {
                errorCheck.Visible = true;
                errorCheck.Text = "The id is not valid";
                return;
            }

            TrackingClient.DotNetWebService.TrackingServiceSoapClient dotNetService = new TrackingClient.DotNetWebService.TrackingServiceSoapClient();
            DotNetWebService.package package = dotNetService.getPackageByUserIdAndPackageId(user.id,packageIdInput);
            if(package == null)
            {
                errorCheck.Visible = true;
                errorCheck.Text = "The id is not valid";
                return;
            }
            if (package.tracking == false)
            {
                routeErrorLabel.Visible = true;
                routeErrorLabel.Text = "The package was not shipped yet";
                return;
            }
            routeTable.Visible = true;
            foreach (DotNetWebService.pair p in package.routes[0].pairs)
            {
                TableRow row = new TableRow();
                TableCell cityCell = new TableCell();
                cityCell.Text = p.city;
                row.Cells.Add(cityCell);
                TableCell timeCell = new TableCell();
                timeCell.Text = TimeUtil.ConvertFromUnixTimestamp(double.Parse(p.registered_time)).ToString();
                row.Cells.Add(timeCell);

                routeTable.Rows.Add(row);
            }
        }

        protected void signOut_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Login.aspx");
        }
    }
}