﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="TrackingClient.Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin</title>
</head>
<body>
    <form id="form1" runat="server">
        <p>
            <asp:Label ID="helloUser" runat="server" Text="Label"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="signOut" runat="server" OnClick="signOut_Click">SignOut</asp:LinkButton>
        </p>
        <br />
        <asp:Button ID="allBtn" runat="server" Text="All Packages" OnClick="allBtn_Click" />
        <asp:Button ID="addBtn" runat="server" Text="Add Package" OnClick="addBtn_Click" />
        <asp:Button ID="deleteBtn" runat="server" Text="Delete Package" OnClick="deleteBtn_Click" />
        <asp:Button ID="registerBtn" runat="server" OnClick="registerClick" Text="Register Package" Width="144px" />
        <asp:Button ID="updateStatusBtn" runat="server" OnClick="updateClick" Text="Update Package Status" Width="171px" />

        <div id="listAllForm" runat="server">
        <br />
        <asp:Table ID="packageTable" runat="server" BackColor="White" BorderColor="Black" 
    BorderWidth="1" ForeColor="Black" GridLines="Both" BorderStyle="Solid" CellPadding="8">
            <asp:TableHeaderRow >
                <asp:TableCell Font-Bold="True">Id</asp:TableCell>
                <asp:TableCell Font-Bold="True">Sender</asp:TableCell>
                <asp:TableCell Font-Bold="True">Receiver</asp:TableCell>
                <asp:TableCell Font-Bold="True">Package name</asp:TableCell>
                <asp:TableCell Font-Bold="True">Description</asp:TableCell>
                <asp:TableCell Font-Bold="True">Sender city</asp:TableCell>
                <asp:TableCell Font-Bold="True">Destination city</asp:TableCell>
                <asp:TableCell Font-Bold="True">Tracking</asp:TableCell>
            </asp:TableHeaderRow>
        </asp:Table>
        <br />
        <br />
        &nbsp;<br />
    </div>

	<div id="addForm" runat="server" class="auto-style1">
		
        <asp:Table ID="Table1" runat="server">
             <asp:TableRow>
                <asp:TableCell>Sender username</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="senderUsernameBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell>Receiver username</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="recUsernameBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Package name</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="nameBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Package description</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="descriptionBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Sender city</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="senderCityBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Destination City</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="destinationCityBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell><asp:Button ID="Button1" runat="server" OnClick="addPackBtn_Click" Text="Add Package" /></asp:TableCell>
             </asp:TableRow>
        </asp:Table>
   
        <asp:Label ID="successAddLabel" runat="server"></asp:Label>
   
        <asp:Label ID="addErrorLabel" runat="server" ForeColor="#FF0066"></asp:Label>
   
        <br />
   
    </div>

	<div id="registerForm" runat="server" class="auto-style2">
	

	    <br />
	
	    <asp:Label ID="Label4" runat="server" Text="Package id: "></asp:Label>
        <asp:TextBox ID="registerPackageIdBox" runat="server"></asp:TextBox>
        <asp:Button ID="registerPackageBtn" runat="server" OnClick="Register_Package_btn" Text="Register for tracking" />
	
	    <br />
	
	    <br />
        <asp:Label ID="registerLabel" runat="server"></asp:Label>
        <asp:Label ID="registerErrorLabel" runat="server" ForeColor="Red"></asp:Label>
	

	</div>
    <div id="updateStatusForm" runat="server" class="auto-style2">
	    <asp:Label ID="Label5" runat="server" Text="Insert time in format: yyyy-MM-dd HH:mm:ss"></asp:Label>
        <br />
	 <asp:Table ID="Table2" runat="server">
             <asp:TableRow>
                <asp:TableCell>Package id</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="pairPackageIdBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell>City Name</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="pairCityBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Time</asp:TableCell>
                <asp:TableCell><asp:TextBox ID="pairTimeBox" runat="server"></asp:TextBox></asp:TableCell>
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell></asp:TableCell>
                <asp:TableCell><asp:Button ID="addPairBtn" runat="server" OnClick="addPairBtn_Click" Text="Add Pair" /></asp:TableCell>
             </asp:TableRow>


         </asp:Table>

	    <br />
        <asp:Label ID="updatedLabel" runat="server"></asp:Label>
        <asp:Label ID="updateErrorLabel" runat="server" ForeColor="Red"></asp:Label>
	
	</div>
    <div id="deleteForm" runat="server" class="auto-style3">
	
	    <br />
        <asp:Label ID="Label3" runat="server" Text="Package id: "></asp:Label>
        <asp:TextBox ID="deletePackageIdBox" runat="server"></asp:TextBox>
        <asp:Button ID="deletePackageBtn" runat="server" OnClick="Delete_Package_btn" Text="Delete" style="height: 26px" />
        <br />
        <br />
        <asp:Label ID="deletedLabel" runat="server"></asp:Label>
        <asp:Label ID="deleteErrorLabel" runat="server" ForeColor="Red"></asp:Label>
	
	    </div>
    </form>
</body>
</html>

