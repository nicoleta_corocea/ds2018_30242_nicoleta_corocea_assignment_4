﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TrackingClient.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
</head>
<body style="height: 228px">
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label>
            <asp:TextBox ID="usernameBox" runat="server" Height="16px" OnTextChanged="TextBox1_TextChanged" style="margin-left: 26px"></asp:TextBox>
            <br />
            <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
            <asp:TextBox ID="passwordBox" runat="server" Height="16px" OnTextChanged="TextBox1_TextChanged" style="margin-left: 26px"></asp:TextBox>
        </div>
        <p>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Login" />
        </p>
        <p>
            <asp:Label ID="errrorLabel" runat="server" ForeColor="Red"></asp:Label>
        </p>
        <p>
            <asp:Button ID="registerBtn" runat="server" OnClick="registerBtn_Click" Text="Register" />
        </p>
    </form>
</body>
 
</html>
