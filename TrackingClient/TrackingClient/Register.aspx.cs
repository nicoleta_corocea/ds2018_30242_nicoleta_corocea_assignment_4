﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TrackingClient
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void saveUserBtn_Click(object sender, EventArgs e)
        {

            string username = regUsernameBox.Text.ToString();
            string password = regPasswordBox.Text.ToString();

            TrackingClient.DotNetWebService.TrackingServiceSoapClient dotNetService = new TrackingClient.DotNetWebService.TrackingServiceSoapClient();

            if (password.Length == 0 || username.Length == 0)
            {
                error.Text = "Missing credentials!";
                return;
            }

            if (password.Length <4)
            { 
                error.Text = "The password is too short";
                return;
            }
            if (username.Length < 4)
            { 
                error.Text = "The username is too short";
                return;
            }
 
            DotNetWebService.user user = null;
            try {
                user = dotNetService.register(regUsernameBox.Text.ToString(), regPasswordBox.Text.ToString());
            }
            catch
            {
                 error.Text = "The webservice is closed. Please turn it on";
                 return;
             }
            if (user != null)
            {
                Session["user"] = user;
                Response.Redirect("Client.aspx");
            }
            else
            {
                error.Text = "Something went wrong. Please try again";
            }
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
    }
}