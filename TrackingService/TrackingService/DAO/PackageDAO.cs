﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace TrackingService.DAO
{
    public class PackageDAO
    {
        internal List<package> getAllPackagesByUserId(int id)
        {
            TrackingDbContext context = new TrackingDbContext();

            return context.packages.Where(p => (p.id_sender == id || p.id_receiver == id))
                    .Include(p => p.sender)
                    .Include(p => p.receiver)
                    .Include(p => p.routes.Select(r => r.pairs))
                    .ToList();
        }

        internal List<package> getAllPackagesByUserIdAndPackageName(int id,string packageName)
        {
            TrackingDbContext context = new TrackingDbContext();

            return context.packages.Where(p => ((p.id_sender == id || p.id_receiver == id)&&p.package_name.Contains(packageName)))
                    .Include(p => p.sender)
                    .Include(p => p.receiver)
                    .Include(p => p.routes.Select(r => r.pairs))
                    .ToList();
        }

        internal package getPackageById(int id)
        {
            var context = new TrackingDbContext();

            package pack = null;
            try
            {
                pack = context.packages.Where(p => (p.id == id))
                    .Include(p=>p.sender)
                    .Include(p=>p.receiver)
                    .Include(p=>p.routes.Select(r => r.pairs))
                    .Single();
            }
            catch (Exception)
            {

            }

            return pack;
        }

        
        internal package getPackageByUserIdAndPackageId(int userId,int packageId)
        {
            var context = new TrackingDbContext();

            package pack = null;
            try
            {
                pack = context.packages.Where(p => (p.id == packageId)&&((p.id_sender == userId)|| (p.id_receiver == userId)))
                    .Include(p => p.sender)
                    .Include(p => p.receiver)
                    .Include(p => p.routes.Select(r => r.pairs))
                    .Single();
            }
            catch (Exception)
            {

            }

            return pack;
        }
    }
}