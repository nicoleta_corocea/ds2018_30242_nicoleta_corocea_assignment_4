﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Serialization;
using TrackingService.DAO;

namespace TrackingService.Service
{
    /// <summary>
    /// Summary description for TrackingService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TrackingService : System.Web.Services.WebService
    {

        [WebMethod]
       // [XmlInclude(typeof(List<package>))]
      //  [XmlInclude(typeof(user))]
        public user login(string username, string password)
        {
            UserDAO userDAO = new UserDAO();
            user user = userDAO.login(username, password);
           // user user = userDAO.login("maria", "maria");
            return user;
        }

        [WebMethod]
        public List<package> getAllPackagesByUserId(int id)
        {
            PackageDAO packageDAO = new PackageDAO();
            List<package> packages= packageDAO.getAllPackagesByUserId(id);
            return packages;
        }

        [WebMethod]
        public List<package> getAllPackagesByUserIdAndPackageName(int id, string packageName)
        {
            PackageDAO packageDAO = new PackageDAO();
            List<package> packages = packageDAO.getAllPackagesByUserIdAndPackageName(id,packageName);
            return packages;
        }

        [WebMethod]
        public user register(string username, string password)
        {
            UserDAO userDAO = new UserDAO();
            user user = userDAO.register(username, password);
            return user;
        }

        [WebMethod]
        public package getPackageById(int id)
        {
            PackageDAO packageDAO = new PackageDAO();
            return packageDAO.getPackageById(id);
        }

        [WebMethod]
        public package getPackageByUserIdAndPackageId(int userId, int packageId)
        {
            PackageDAO packageDAO = new PackageDAO();
            return packageDAO.getPackageByUserIdAndPackageId(userId, packageId);
        }

        [WebMethod]
        public route getPackageRouteDetails(int id)
        {
            RouteDAO routeDAO = new RouteDAO();
            return routeDAO.getPackageRouteDetails(id);
        }
    }
}
