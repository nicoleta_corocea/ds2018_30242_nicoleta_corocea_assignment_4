package tracking.operation;

import java.util.List;

import tracking.dao.RouteDAO;
import tracking.model.Route;

public class RouteServiceImpl implements RouteService {

	private RouteDAO routeDAO = RouteDAO.getInsance();

	public List<Route> getAllRoutes() {
		return routeDAO.getAllRoutes();
	}

	public Route removeRoute(int id) {
		return routeDAO.removeRoute(id);
	}

	public int createRoute(int id) {
		return routeDAO.createRoute(id);
	}

	private static RouteServiceImpl routeServiceImpl = null;

	private RouteServiceImpl() {

	}

	public static RouteServiceImpl getInsance() {
		if (routeServiceImpl == null) {
			return new RouteServiceImpl();
		}
		return routeServiceImpl;
	}

}
