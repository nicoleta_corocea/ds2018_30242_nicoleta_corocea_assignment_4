package tracking.operation;

import tracking.dao.PairDAO;
import tracking.dao.RouteDAO;
import tracking.model.Pair;
import tracking.model.Route;

public class PairsServiceImpl implements PairsService {

	private PairDAO pairDAO = PairDAO.getInsance();
	private RouteDAO routeDAO = RouteDAO.getInsance();

	public int addPair(int packageId, String cityName, long unixtimestamp) {

		Route route = routeDAO.getRouteByPackageId(packageId);

		if (route != null) {
			Pair pair = new Pair();
			pair.setCity(cityName);
			pair.setTime(unixtimestamp);
			pair.setRouteId(route.getId());
			return pairDAO.addPair(pair);
		}
		return -1;
	}

	private static PairsServiceImpl pairsServiceImpl = null;

	private PairsServiceImpl() {

	}

	public static PairsServiceImpl getInsance() {
		if (pairsServiceImpl == null) {
			return new PairsServiceImpl();
		}
		return pairsServiceImpl;
	}

}
