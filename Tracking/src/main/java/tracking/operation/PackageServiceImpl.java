package tracking.operation;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import tracking.dao.PackageDAO;
import tracking.dao.RouteDAO;
import tracking.dao.UserDAO;
import tracking.model.Package;
import tracking.model.Route;
import tracking.model.User;
import tracking.util.HibernateUtil;

public class PackageServiceImpl implements PackageService {

	private PackageDAO packageDAO = PackageDAO.getInsance();
	private RouteDAO routeDAO = RouteDAO.getInsance();
	private UserDAO userDAO = UserDAO.getInsance();

	public int addPackage(Package p) {
		//User sender = userDAO.getUserById(p.getSender().getId());
		//User receiver = userDAO.getUserById(p.getReceiver().getId());
		
		User sender = userDAO.getUserByUsername(p.getSender().getUserName());
		User receiver = userDAO.getUserByUsername(p.getReceiver().getUserName());
		if (sender == null || receiver == null) {
			return -1;
		}
		p.setSender(sender);
		p.setReceiver(receiver);
		return packageDAO.addPackage(p);
	}

	public Package removePackage(int id) {
		Route route = routeDAO.removeRoute(id);
		Package removed = packageDAO.removePackage(id);

		return removed;
	}

	public Package registerPackage(int id) {
		Package registerdP = packageDAO.registerPackage(id);
		if (registerdP != null && routeDAO.getRouteByPackageId(id) == null) {
			routeDAO.createRoute(id);
			return registerdP;
		}

		return null;
	}

	public List<Package> getAllPackages() {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		List<Package> packages = session.createCriteria(Package.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

		session.getTransaction().commit();
		session.close();
		return packages;
	}

	private static PackageServiceImpl packageServiceImpl = null;

	private PackageServiceImpl() {

	}

	public static PackageServiceImpl getInsance() {
		if (packageServiceImpl == null) {
			return new PackageServiceImpl();
		}
		return packageServiceImpl;
	}

}
