package tracking.operation;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import tracking.model.Package;
import tracking.model.Pair;

@WebService
public interface TrackingWebService {

	@WebMethod
	public List<Package> getAllPackages();

	@WebMethod
	public int addPackage(Package p);

	@WebMethod
	public Package removePackage(int id);

	@WebMethod
	public Package registerPackage(int id);

	@WebMethod
	public int addPairToPackageRoute(int packageId, String cityName, long unixTimestamp);

}
