package tracking.operation;

import java.util.List;

import tracking.model.Route;

public interface RouteService {

	public List<Route> getAllRoutes();

	public Route removeRoute(int id);

	public int createRoute(int id);

}
