package tracking.operation;

import java.util.List;

import tracking.model.Package;

public interface PackageService {

	public List<Package> getAllPackages();

	public int addPackage(Package p);

	public Package removePackage(int id);

	public Package registerPackage(int id);
}
