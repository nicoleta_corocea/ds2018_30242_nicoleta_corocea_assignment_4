package tracking.operation;

import java.util.List;

import javax.jws.WebService;

import tracking.model.Package;
import tracking.model.Pair;

@WebService(endpointInterface = "tracking.operation.TrackingWebService")
public class TrackingWebServiceImpl implements TrackingWebService {

	private PackageServiceImpl packageService = PackageServiceImpl.getInsance();
	private PairsServiceImpl pairsService = PairsServiceImpl.getInsance();

	public List<Package> getAllPackages() {
		return packageService.getAllPackages();
	}

	public int addPackage(Package p) {
		return packageService.addPackage(p);
	}

	public Package removePackage(int id) {
		return packageService.removePackage(id);
	}

	public Package registerPackage(int id) {
		return packageService.registerPackage(id);
	}

	public int addPairToPackageRoute(int packageId, String cityName, long unixTimestamp) {
		return pairsService.addPair(packageId, cityName, unixTimestamp);
	}

}
