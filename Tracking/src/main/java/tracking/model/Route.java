package tracking.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "routes")
public class Route {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "id_package")
	private Integer packageId;

	// @OneToMany(mappedBy = "routeId", fetch = FetchType.EAGER, cascade =
	// CascadeType.REMOVE, orphanRemoval = true)
	// @Column(name = "id_route")
	/*
	 * @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval
	 * = true)
	 * 
	 * @JoinColumn(name = "id_route")
	 */

	@OneToMany(mappedBy = "routeId", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	// @Column(name = "id_route")
	private Set<Pair> pairs = new HashSet<Pair>();

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	public Route(Integer packageId) {
		this.packageId = packageId;
	}

	public Route() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Pair> getPairs() {
		return pairs;
	}

	public void setPairs(Set<Pair> pairs) {
		this.pairs = pairs;
	}

	@Override
	public String toString() {
		return "Route [id=" + id + ", packageId=" + packageId + ", pairs=" + pairs + "]";
	}

}
