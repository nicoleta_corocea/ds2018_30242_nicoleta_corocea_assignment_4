package tracking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "packages")
public class Package {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@OneToOne(targetEntity = User.class)
	@JoinColumn(name = "id_sender")
	private User sender;

	@OneToOne(targetEntity = User.class)
	@JoinColumn(name = "id_receiver")
	private User receiver;

	@Column(name = "package_name", nullable = false)
	private String packageName;

	@Column(name = "package_description", nullable = false)
	private String packageDescription;

	@Column(name = "sender_city", nullable = false)
	private String senderCity;

	@Column(name = "destination_city", nullable = false)
	private String destinationCity;

	@Column(name = "tracking", nullable = false)
	private boolean tracking;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageDescription() {
		return packageDescription;
	}

	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

	public Package(User sender, User receiver, String packageName, String packageDescription, String senderCity,
			String destinationCity, boolean tracking) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.packageName = packageName;
		this.packageDescription = packageDescription;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.tracking = tracking;
	}

	public Package() {
		super();
	}

}
