package tracking.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pairs")
public class Pair {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "registered_time", nullable = false)
	private Long time;

	@Column(name = "id_route", nullable = false)
	private Integer routeId;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public Pair(String city, Long time, Integer routeId) {
		super();
		this.city = city;
		this.time = time;
		this.routeId = routeId;
	}

	public Pair() {
		super();
	}

}
