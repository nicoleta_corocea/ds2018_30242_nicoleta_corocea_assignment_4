package tracking.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tracking.model.Route;
import tracking.util.HibernateUtil;

public class RouteDAO {
	private static RouteDAO routeDAO = null;

	private RouteDAO() {

	}

	public static RouteDAO getInsance() {
		if (routeDAO == null) {
			return new RouteDAO();
		}
		return routeDAO;
	}

	public List<Route> getAllRoutes() {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		List<Route> routes = session.createCriteria(Route.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();

		session.getTransaction().commit();
		session.close();
		return routes;
	}

	public Route removeRoute(int id) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Criteria routeCriteria = session.createCriteria(Route.class).add(Restrictions.eq("packageId", id));
		Route route = (Route) routeCriteria.uniqueResult();
		if (route != null) {
			session.remove(route);
		}
		session.getTransaction().commit();
		session.close();

		return route;
	}

	public int createRoute(int packageId) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Integer id = (Integer) session.save(new Route(packageId));

		session.getTransaction().commit();
		session.close();
		return id;
	}

	public Route getRouteById(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Criteria criteria = session.createCriteria(Route.class);
		Route route = (Route) criteria.add(Restrictions.eq("id", id)).uniqueResult();
		session.getTransaction().commit();

		return route;
	}

	public Route getRouteByPackageId(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Criteria criteria = session.createCriteria(Route.class);
		Route route = (Route) criteria.add(Restrictions.eq("packageId", id)).uniqueResult();
		session.getTransaction().commit();

		return route;
	}
}
