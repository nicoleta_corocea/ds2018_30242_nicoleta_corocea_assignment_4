package tracking.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tracking.model.Package;
import tracking.util.HibernateUtil;

public class PackageDAO {

	// private RouteDAO routeDAO = RouteDAO.getInsance();

	private static PackageDAO packageDAO = null;

	private PackageDAO() {

	}

	public static PackageDAO getInsance() {
		if (packageDAO == null) {
			return new PackageDAO();
		}
		return packageDAO;
	}

	public int addPackage(Package p) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Integer id = (Integer) session.save(p);

		session.getTransaction().commit();
		session.close();
		return id;
	}

	public Package removePackage(int id) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Package.class).add(Restrictions.eq("id", id));
		Package p = (Package) criteria.uniqueResult();

		if (p != null) {
			session.delete(p);
		}

		session.getTransaction().commit();
		session.close();

		return p;
	}
/**
 * 
 * @param id
 * updates package tracking flag to true
 * @return updated package or null if the id does not exist
 */
	public Package registerPackage(int id) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Package.class).add(Restrictions.eq("id", id));
		Package p = (Package) criteria.uniqueResult();

		if (p != null) {
			session.evict(p);
			p.setTracking(true);
			session.update(p);
		}
		session.getTransaction().commit();
		session.close();
		return p;
	}
	

}
