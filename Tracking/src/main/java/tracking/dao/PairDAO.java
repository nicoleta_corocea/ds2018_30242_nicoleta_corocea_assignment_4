package tracking.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import tracking.model.Pair;
import tracking.util.HibernateUtil;

public class PairDAO {
	private static PairDAO pairDAO = null;

	private PairDAO() {

	}

	public static PairDAO getInsance() {
		if (pairDAO == null) {
			return new PairDAO();
		}
		return pairDAO;
	}

	public int addPair(Pair pair) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Integer id = (Integer) session.save(pair);

		session.getTransaction().commit();
		session.close();
		return id;
	}
}
