package tracking.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import tracking.model.User;
import tracking.util.HibernateUtil;

public class UserDAO {
	private static UserDAO userDAO = null;

	private UserDAO() {

	}

	public static UserDAO getInsance() {
		if (userDAO == null) {
			return new UserDAO();
		}
		return userDAO;
	}

	public int addUser(User user) {
		SessionFactory sesionFactory = HibernateUtil.getSessionFactory();
		Session session = sesionFactory.openSession();
		session.beginTransaction();

		Integer id = (Integer) session.save(user);

		session.getTransaction().commit();
		session.close();
		return id;
	}

	public User getUserById(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Criteria criteria = session.createCriteria(User.class);
		User user = (User) criteria.add(Restrictions.eq("id", id)).uniqueResult();
		session.getTransaction().commit();

		return user;
	}

	public User getUserByUsername(String userName) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Criteria criteria = session.createCriteria(User.class);
		User user = (User) criteria.add(Restrictions.eq("userName", userName)).uniqueResult();
		session.getTransaction().commit();

		return user;
	}
}
