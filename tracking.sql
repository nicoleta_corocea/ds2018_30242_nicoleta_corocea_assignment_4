drop database tracking;

create database tracking;
use tracking;


CREATE TABLE users (
    id int auto_increment,
    username varchar(20) NOT NULL,
    password varchar(256) NOT NULL,
    usertype varchar(20) NOT NULL,
	primary key(id)
);



CREATE TABLE packages (
    id int auto_increment,
    id_sender int NOT NULL,
	id_receiver int NOT NULL,
    package_name varchar(20) NOT NULL,
    package_description varchar(60) NOT NULL,
    sender_city varchar(20) NOT NULL,
    destination_city varchar(20) NOT NULL,
    tracking boolean NOT NULL,
    foreign key (id_sender) references users(id),
    foreign key (id_receiver) references users(id),
	primary key(id)
);

CREATE TABLE routes(
	id int auto_increment,
    id_package int NOT NULL UNIQUE,
    foreign key (id_package) references packages(id),
	primary key(id)
);

CREATE TABLE pairs(
    id int auto_increment,
	id_route int NOT NULL,
    city varchar(20) NOT NULL,
    registered_time long NOT NULL,
	foreign key (id_route) references routes(id),
	primary key(id)
);

insert into users(username, password,usertype) values("admin","admin","admin");
insert into users(username, password,usertype) values("maria","maria","client");
insert into users(username, password,usertype) values("ana","ana","client");
    
insert into packages(id_sender,id_receiver,package_name,package_description,sender_city,destination_city,tracking) values (2,3,"aa","bb","cj","vl",true);
insert into routes(id_package) values(1);
insert into pairs(id_route,city,registered_time) values(1,"ab",1541980800);
insert into pairs(id_route,city,registered_time) values(1,"sb",1541988800);
insert into packages(id_sender,id_receiver,package_name,package_description,sender_city,destination_city,tracking) values (3,2,"new","asssd","nt","ab",false);
insert into packages(id_sender,id_receiver,package_name,package_description,sender_city,destination_city,tracking) values (3,2,"tit","ne","ag","zv",false);

select * from routes;
select * from pairs;
select * from packages;
select * from users;